const express = require('express');
const app = express();
const port = 3030;
const DataModel = require('./data');

const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://iotscale:ryMVDaALhRtA47Y2@cluster0.3fam2et.mongodb.net/scale?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(express.json());

app.get('/', async (req, res) => {
  try {
    const data = await DataModel.find({}, '-_id id ws time').sort({ time: -1 });
    res.status(200).json({ code: 200, message: 'success', data });
  } catch (error) {
    res.status(400).json({ code: 400, message: 'error' });
  }
});

app.post('/', async (req, res) => {
  try {
    const data = req.body;
    for (dat of data) {
      const { ws, id } = dat;
      const newData = new DataModel({ id, ws, time: new Date() });
      await newData.save();
    }

    console.log(data);
    res.status(200).json({ code: 200, message: 'success', data });
  } catch (error) {
    res.status(400).json({ code: 400, message: 'error' });
  }
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
