const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema(
  {
    id: String,
    ws: Number,
    time: Date,
  },
  { versionKey: false }
);

const DataModel = mongoose.model('Data', dataSchema, 'data');
module.exports = DataModel;
